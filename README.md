# TDS_HIOS_manuscript



## Code and datasets for DOI 10.1002/pssa.202300148
* The here uploaded Jupyter Notebooks belong to the publication tilted "Probing crystallinity and grain structure of 2D materials and 2D-like van der Waals heterostructures by low-voltage electron diffraction" - DOI: [10.1002/pssa.202300148
202300148](https://doi.org/10.1002/pssa.202300148) 
* The Jupyter Notebooks analyze the datasets and generate all the figures in this publication
    * They were tested with Jupyter Lab (version 3.5.2) on Windows 10
* The 4D-STEM datasets and images are available here: [https://doi.org/10.5281/zenodo.8100688](https://doi.org/10.5281/zenodo.8100688) 
    * Unzip and move these files in the "data" folder to run the Jupyter Notebooks.

